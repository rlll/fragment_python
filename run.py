"""
    Утилита запуска выполняемых процессов
"""

import argparse

from process_engine import Runnable
from process_engine import set_engine_run_args, get_engine_run_runnable_instance, get_engine_run_configuration

HAS_APP = True
try:
    from app import System
    from app import set_app_run_args, get_app_run_runnable_instance
except ImportError:
    HAS_APP = False
    from process_engine import System

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--version", action="version",
                        version="{system_name} {system_version} {utility_name}".format(
                            system_name=System.name,
                            system_version=System.version,
                            utility_name="service runner"
                        ),
                        help="show version and exit")

    subparsers = parser.add_subparsers(metavar="<runnable>", dest="runnable_name", help="specify service")
    subparsers.required = True

    set_engine_run_args(subparsers)
    if HAS_APP:
        set_app_run_args(subparsers)

    # optional arguments
    parser.add_argument("-s", "--source", metavar="<source-uri>", default=None, required=False,
                        help="set configuration source uri")
    parser.add_argument("-g", "--group", metavar="<group-name>", default=None, required=False,
                        help="set configuration group name")
    parser.add_argument("-c", "--conf", metavar="<config-key>", default=None, required=False,
                        help="set configuration key")

    args = parser.parse_args()

    runnable, runnable_name = get_engine_run_runnable_instance(args.runnable_name, System.prefix, args)
    if runnable is None:
        if HAS_APP:
            runnable, runnable_name = get_app_run_runnable_instance(args.runnable_name, System.prefix, args)
            if runnable is None:
                runnable = Runnable("unknown")
        else:
            runnable = Runnable("unknown")

    configuration, error_text = get_engine_run_configuration(runnable_name,
                                                             source_uri=args.source,
                                                             group=args.group,
                                                             config_key=args.conf)
    if configuration is not None:
        if runnable.init(configuration):
            runnable.run()
        else:
            print("Error: {error_text}".format(error_text=runnable.last_error))
    else:
        print("Error: {error_text}".format(error_text=error_text))
