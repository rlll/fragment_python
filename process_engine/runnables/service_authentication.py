"""
    Сервис REST API для аутентификации и выдачи токенов и разрешений
"""

from process_engine import ServiceRunnable
from process_engine.service_apps import ServiceAppAuthentication
from process_engine.database import AuthDb


class ServiceAuthentication(ServiceRunnable):
    def __init__(self, name, prefix=""):
        super().__init__(name, prefix=prefix)
        self.db = None

    def additional_init(self):
        super().additional_init()

        # db setup
        if "db" not in self.config:
            self.log("error", "System", "There is no configuration for database")
            raise Exception("There is no configuration for database")

    def runtime_cycle(self):
        self.db = AuthDb()
        self.db.init(self.config["db"]["host"], self.config["db"]["port"], self.config["db"]["name"])
        self.log("info", "Database", "Connecting to",
                 host=self.config["db"]["host"],
                 port=self.config["db"]["port"],
                 name=self.config["db"]["name"]
        )
        self.app = ServiceAppAuthentication(self.db, self.log).get_app()

        super().runtime_cycle()

    def stop(self):
        self.db.close()
        super().stop()
