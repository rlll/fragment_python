"""
    Абстрактный класс для Web-сервисов
"""


class ServiceApp(object):
    def __init__(self, db=None):
        self.app = None
        self.db = db
        self.header_token = "X-PE-Token"

    class TokenNotFoundException(Exception):
        pass

    class NotAuthorizedException(Exception):
        pass

    def get_permission(self, headers, subsystem):
        if self.header_token in headers:
            token = headers[self.header_token]
        else:
            raise self.TokenNotFoundException("Header '{header}' is required".format(header=self.header_token))

        authorized = self.db.refresh_token(token)
        if authorized:
            level = self.db.get_permission(token, subsystem)
        else:
            raise self.NotAuthorizedException("Token is expired")
        return level

    def get_app(self):
        return self.app
