"""
    Класс процесса сервиса
"""


from process_engine import AsyncMQRunnable


class ServiceRunnable(AsyncMQRunnable):
    def __init__(self, name, prefix=""):
        super().__init__(name, prefix=prefix)

        self.app = None
        self.http_server = None

    def additional_init(self):
        super().additional_init()

        # port setup
        if "port" not in self.config:
            self.log("error", "System", "There is no configuration for listen port")
            raise Exception("There is no configuration for listen port")

    def runtime_cycle(self):
        if self.app is not None:
            self.http_server = self.app.listen(int(self.config["port"]))
            self.log("info", "Runtime", "Listening service at", port=int(self.config["port"]))

            super().runtime_cycle()

    def stop(self):
        self.log("info", "Runtime", "Stopping service")
        self.http_server.stop()

        super().stop()
