"""
    Класс ответа
"""


class Response(object):
    def __init__(self, is_error=False):
        self.to = None
        self.body = None
        self.is_error = is_error

        self.reply_id = None