"""
    Класс процесса
"""

import logging
import os
import platform
import uuid
from logging.handlers import RotatingFileHandler
import signal

from process_engine.loggers import MongoLogger
from process_engine.system import System
from process_engine.configuration_reader import ConfigurationReader


class Runnable(object):
    def __init__(self, name, prefix=""):
        self.prefix = prefix
        self.name = name
        self.service_name = None
        self.instance_service_name = None
        self.hostname = platform.uname()[1]
        self.pid = os.getpid()
        self.config = None
        self.last_error = ""
        self.file_logger = None
        self.db_logger = None

    def init(self, configuration):
        self.service_name = "{prefix}.{name}".format(prefix=self.prefix, name=self.name)
        self.instance_service_name = "{service_name}.{uuid}".format(service_name=self.service_name,
                                                                    uuid=str(uuid.uuid4()))
        self.config = configuration

        if self.config is None:
            self.config = ConfigurationReader(None)

        try:
            self.config.parse()
        except Exception as ex:
            self.last_error = str(ex)
            return False

        # logging setup
        if "logging" not in self.config:
            self.config["logging"] = dict()
            self.config["logging"]["file"] = False
            self.config["logging"]["db"] = False
        if "file" in self.config["logging"] and self.config["logging"]["file"]:
            if type(self.config["logging"]["file"]) == bool:
                self.config["logging"]["file"] = {}
            self.file_logger = logging.getLogger("app")
            self.file_logger.setLevel(logging.INFO)
            rotating_handler = logging.handlers.RotatingFileHandler(
                "{log_path}/{filename}.{log_ext}".format(
                    log_path=System.log_path,
                    filename=self.service_name,
                    log_ext=System.log_ext
                ),
                maxBytes=System.log_default_size
                    if "size" not in self.config["logging"]["file"] else self.config["logging"]["file"]["size"],
                backupCount=System.log_backup_count
                    if "backup-count" not in self.config["logging"]["file"] else self.config["logging"]["file"]["backup-count"],
                encoding="utf-8"
            )
            rotating_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s: %(message)s"))
            self.file_logger.addHandler(rotating_handler)

        if "db" in self.config["logging"] and \
            self.config["logging"]["db"] and \
            "host" in self.config["logging"]["db"] and \
            "port" in self.config["logging"]["db"] and \
            "name" in self.config["logging"]["db"]:
            self.db_logger = MongoLogger()
            try:
                if "group" in self.config["logging"]["db"]:
                    self.db_logger.init(self.config["logging"]["db"]["host"], self.config["logging"]["db"]["port"],
                                        self.config["logging"]["db"]["name"],
                                        group=self.config["logging"]["db"]["group"])
                else:
                    self.db_logger.init(self.config["logging"]["db"]["host"], self.config["logging"]["db"]["port"],
                                        self.config["logging"]["db"]["name"])
            except Exception as ex:
                self.last_error = str(ex)
                return False

        self.log("info", "System", "Reading configuration", config_path=self.config.description)

        # additional init
        try:
            self.additional_init()
        except Exception as ex:
            self.last_error = str(ex)
            return False

        return True

    def log_text(self, tag, text, **kwargs):
        full_text = "[{tag}] (service={service}, instance={instance}, pid={pid}) {text} ({data})".format(
            tag=tag,
            service=self.service_name,
            instance=self.instance_service_name,
            pid=self.pid,
            text=text,
            data=",".join(["{key}={value}".format(key=k, value=kwargs[k]) for k in kwargs])
        )
        if len(kwargs) == 0:
            full_text = full_text.replace("()", "")
        return full_text

    def log_dict(self, tag, text, **kwargs):
        full_dict = dict({
            "tag": tag,
            "service": self.service_name,
            "instance": self.instance_service_name,
            "host": self.hostname,
            "pid": self.pid,
            "text": text,
            "data": {}
        })
        for key in kwargs:
            full_dict["data"][key] = kwargs[key]
        return full_dict

    def log(self, severity, tag, text, **kwargs):
        if self.file_logger is not None:
            if severity == "info":
                self.file_logger.info(self.log_text(tag, text, **kwargs))
            elif severity == "warning":
                self.file_logger.warning(self.log_text(tag, text, **kwargs))
            elif severity == "error":
                self.file_logger.error(self.log_text(tag, text, **kwargs))
            else:
                self.file_logger.debug(self.log_text(tag, text, **kwargs))
        if self.db_logger is not None:
            self.db_logger.log(severity, self.log_dict(tag, text, **kwargs))

    def additional_init(self):
        pass

    def runtime_cycle(self):
        pass

    def run(self):
        self.log("info", "System", "Starting with configuration", config_path=self.config.description)

        this = self

        def signal_handler(signum, frame):
            print(signum, frame)
            this.stop()

        if os.name == "nt":
            try:
                self.runtime_cycle()
            except KeyboardInterrupt:
                self.stop()
        else:
            signal.signal(signal.SIGINT, signal_handler)
            self.runtime_cycle()
        self.log("info", "System", "Stopped")
        if self.db_logger is not None:
            self.db_logger.close()

    def stop(self):
        pass

if __name__ == "__main__":
    os.chdir("../")

    runnable = Runnable("worker-test", prefix="uaas")
    if runnable.init(None):
        print("Service name:", "{prefix}.{name}".format(prefix=runnable.prefix, name=runnable.name))
        print("Hostname:", runnable.hostname)
        print("Process PID:", runnable.pid)
        print("Configuration:", runnable.config)
        print("File logger:", runnable.file_logger)
        print("DB logger:", runnable.db_logger)

        runnable.run()
    else:
        print("Error:", runnable.last_error)


