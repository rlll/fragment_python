"""
    Работа с БД авторизации
"""

import datetime
import pymongo
import uuid

from enum import IntEnum
from bson import ObjectId
from pymongo import MongoClient
from pymongo.results import InsertOneResult

from process_engine import System
from pymongo.errors import ConnectionFailure


class AuthDb(object):
    class Permission(IntEnum):
        UNKNOWN = -1
        NOT_ALLOWED = 0
        READ = 1
        WRITE = 2

    def __init__(self):
        self.client = None
        self.db = None

    def init(self, host, port, name):
        try:
            self.client = MongoClient("mongodb://{host}:{port}/".format(host=host, port=port))
            self.db = self.client[name]
        except ConnectionFailure:
            raise Exception("Can't connect to authentication database on {host}:{port}".format(host=host, port=port))

    def refresh_token(self, token):
        """
        Возвращает:
        - False, если токен не найден
        -  True, если дата создания токена обновлена
        """
        result = False
        if token is not None:
            write_result = self.db["tokens"].update({"token": token}, {"$set": {
                "date_create": datetime.datetime.utcnow()}})
            result = write_result["updatedExisting"]
        return result

    def get_user_by_login(self, login):
        result = {}
        if login is not None:
            query = {"login": login}
            doc = self.db["users"].find_one(query)
            if doc is not None:
                result = doc
        return result

    def get_permission(self, token, subsystem):
        """
        Возвращает:
        - -1, если не найден ключ "permissions" или ключ "subsystem"
        - значение ключа "subsystem" из "permissions" токена, в остальных случаях
        """
        result = AuthDb.Permission.UNKNOWN
        if token is not None:
            query = {"token": token}
            doc = self.db["tokens"].find_one(query)
            if doc is not None:
                if "permissions" in doc.keys():
                    permissions = doc["permissions"]
                    if subsystem in permissions.keys():
                        result = permissions[subsystem]
        return int(result)

    def get_permissions(self, token):
        """
        Возвращает:
        - {}, если не найден ключ "permissions"
        - значение ключа "permissions" токена, в остальных случаях
        """
        result = {}
        if token is not None:
            query = {"token": token}
            doc = self.db["tokens"].find_one(query)
            if doc is not None:
                if "permissions" in doc.keys():
                    result = doc["permissions"]
        return result

    def get_role_by_id(self, role_id):
        result = {}
        if role_id is not None:
            query = {"_id": role_id}
            doc = self.db["roles"].find_one(query)
            if doc is not None:
                result = doc
        return result

    def get_permissions_by_role_id(self, role_id):
        result = {}
        if role_id is not None:
            doc = self.get_role_by_id(role_id)
            if doc is not None:
                if "permissions" in doc.keys():
                    result = doc["permissions"]
        return result

    def insert_token_for_user(self, user):
        result = ""
        if user is not None:
            token = uuid.uuid4().hex
            permissions = self.get_permissions_by_role_id(user["role_id"])
            write_result = self.db["tokens"].insert_one(
                {"token": token, "user_id": user["_id"], "date_create": datetime.datetime.utcnow(),
                 "permissions": permissions})
            if type(write_result) is InsertOneResult:
                result = token
        return result

    def delete_token(self, token):
        result = 0
        if token is not None:
            query = {"token": token}
            write_result = self.db["tokens"].remove(query)
            result = write_result["n"]
        return result

    def get_roles_name(self):
        result = []
        cursor = self.db["roles"].find({}, {"name": 1, "_id": 0}).sort("name", pymongo.ASCENDING)
        for doc in cursor:
            result.append(doc["name"])
        return result

    def get_role_by_name(self, role_name):
        result = {}
        if role_name is not None:
            query = {"name": role_name}
            doc = self.db["roles"].find_one(query)
            if doc is not None:
                result = doc
        return result

    def create_role(self, role_name, permissions=None):
        result = 0
        if role_name is not None:
            if self.get_role_by_name(role_name) == {}:
                role = dict({})
                role["name"] = role_name
                if permissions is None:
                    permissions = {}
                    for subsystem in System.subsystems:
                        permissions[subsystem] = int(AuthDb.Permission.NOT_ALLOWED)
                role["permissions"] = permissions
                write_result = self.db["roles"].insert_one(role)
                if type(write_result) is InsertOneResult:
                    result = 1
        return result

    def is_role_used(self, role_name):
        result = False
        if role_name is not None:
            role = self.get_role_by_name(role_name)
            if role != {}:
                query = {"role_id": role["_id"]}
                if self.db["users"].find(query).count() > 0:
                    result = True
        return result

    def delete_role(self, role_name):
        result = 0
        if role_name is not None:
            if not self.is_role_used(role_name):
                query = {"name": role_name}
                write_result = self.db["roles"].remove(query)
                result = write_result["n"]
            else:
                result = -1
        return result

    def update_role(self, role_name, **kwargs):
        result = 0
        if role_name is not None:
            sets = {}
            if "upsert_permissions" in kwargs.keys():
                upsert_permissions = kwargs["upsert_permissions"]
                for key in upsert_permissions:
                    sets["permissions.{subsystem_name}".format(subsystem_name=key)] = AuthDb.Permission.__members__[
                        upsert_permissions[key]].value
            if sets != {}:
                query = {"name": role_name}
                write_result = self.db["roles"].update(query, {"$set": sets})
                result = write_result["n"]
        return result

    def get_users_login(self):
        result = []
        cursor = self.db["users"].find({}, {"login": 1, "_id": 0}).sort("login", pymongo.ASCENDING)
        for doc in cursor:
            result.append(doc["login"])
        return result

    def create_user(self, login, password=None, access_list=None, role_id=None):
        result = 0
        if login is not None:
            if self.get_user_by_login(login) == {}:
                user = dict({})
                user["login"] = login
                user["date_create"] = datetime.datetime.utcnow()
                if password is not None:
                    user["password"] = password
                if access_list is not None:
                    user["access_list"] = access_list
                else:
                    user["access_list"] = []
                if role_id is not None:
                    user["role_id"] = role_id
                else:
                    user["role_id"] = ""
                write_result = self.db["users"].insert_one(user)
                if type(write_result) is InsertOneResult:
                    result = 1
        return result

    def is_user_connected(self, login):
        result = False
        if login is not None:
            user = self.get_user_by_login(login)
            if user != {}:
                query = {"user_id": user["_id"]}
                if self.db["tokens"].find(query).count() > 0:
                    result = True
        return result

    def delete_user(self, login):
        result = 0
        if login is not None:
            if not self.is_user_connected(login):
                query = {"login": login}
                write_result = self.db["users"].remove(query)
                result = write_result["n"]
            else:
                result = -1
        return result

    def update_user(self, login, **kwargs):
        result = 0
        if login is not None:
            if not self.is_user_connected(login):
                sets = {}
                if "role_name" in kwargs.keys():
                    role = self.get_role_by_name(kwargs["role_name"])
                    if "_id" in role.keys():
                        sets["role_id"] = role["_id"]
                if "password" in kwargs.keys():
                    sets["password"] = kwargs["password"]
                if "set_ip" in kwargs.keys():
                    sets["access_list"] = kwargs["set_ip"]
                if "add_ip" in kwargs.keys() or "remove_ip" in kwargs.keys():
                    if "access_list" not in sets.keys():
                        user = self.get_user_by_login(login)
                        if "access_list" in user.keys():
                            sets["access_list"] = user["access_list"]
                        else:
                            sets["access_list"] = []
                    if "add_ip" in kwargs.keys():
                        for ip in kwargs["add_ip"]:
                            sets["access_list"].append(ip)
                    if "remove_ip" in kwargs.keys():
                        for ip in kwargs["remove_ip"]:
                            sets["access_list"].remove(ip)
                if sets != {}:
                    query = {"login": login}
                    write_result = self.db["users"].update(query, {"$set": sets})
                    result = write_result["n"]
            else:
                result = -1
        return result

    def close(self):
        if self.client is not None:
            self.client.close()
