"""
    Реализация REST Web-сервиса для авторизации
"""

import base64
import hashlib
import hmac
import pyrestful

from process_engine import ServiceApp
from pyrestful import mediatypes
from pyrestful.rest import get
from pyrestful.rest import delete


class ServiceAppAuthentication(ServiceApp):
    def __init__(self, db=None, log=None):
        super().__init__()
        self.service_name = "Authentication"
        self.db = db
        self.log = log
        self.header_authorization = "Authorization"
        self.authentication_type = "hmac"
        self.encoding = "utf-8"
        self.header_token = "X-PE-Token"

    class LoginHandler(pyrestful.rest.RestHandler):
        def initialize(self, app):
            self.app = app

        @get(_path="/login", _produces=mediatypes.APPLICATION_JSON)
        def login(self):
            try:
                if self.app.header_authorization in self.request.headers:
                    request_header_authorization = self.request.headers[self.app.header_authorization].split(" ")
                    if request_header_authorization[0] == self.app.authentication_type:
                        login, salt, digest = request_header_authorization[1].split(":")
                        if login is None or salt is None or digest is None:
                            raise Exception("Wrong format of authentication string")
                    else:
                        raise Exception(
                            "Authentication type '{at}' is required".format(at=self.app.authentication_type))
                else:
                    raise Exception("Header '{header}' is required".format(header=self.app.header_authorization))
            except Exception as ex:
                self.app.log("warning", self.app.service_name,
                             "Bad login request: Access is denied due to invalid request",
                             exeption=ex, request=self.request)
                self.gen_http_error(400, "Bad request: The request must include a '{header}' " +
                                    "header field".format(headre=self.app.header_authorization))
                return

            try:
                concatenated = self.request.method + "+" + self.request.uri + "+" + salt
                user = self.app.db.get_user_by_login(login)
                if user is not None:
                    if False and "access_list" in user.keys() and len(user["access_list"]) > 0:
                        x_real_ip = self.request.headers.get("X-Real-IP")
                        remote_ip = x_real_ip or self.request.remote_ip
                        if remote_ip not in user["access_list"]:
                            raise Exception
                else:
                    raise Exception("User is not found")
                if "password" in user.keys():
                    password = user["password"]
                else:
                    raise Exception("Password is not set for user")

                gen_digest = base64.b64encode(
                    hmac.new(password.encode(self.app.encoding), concatenated.encode(self.app.encoding),
                             hashlib.sha256).digest())
                self.app.log("debug", self.app.service_name, "Generated digest",
                             gen_digest=gen_digest.decode(self.app.encoding))

                if gen_digest == digest.encode(self.app.encoding):
                    token = self.app.db.insert_token_for_user(user)
                    self.app.log("info", self.app.service_name, "Authenticated", user=login, token=token)
                    return dict({"status": "success", "token": token})
                else:
                    raise Exception("Invalid credentials")
            except Exception as ex:
                self.app.log("warning", self.app.service_name,
                             "Unauthenticated: Access is denied due to invalid credentials",
                             exeption=ex, request=self.request)
                self.gen_http_error(401, "Unauthenticated: Access is denied due to invalid credentials")
                return

    class LogoutHandler(pyrestful.rest.RestHandler):
        def initialize(self, app):
            self.app = app

        @delete(_path="/logout", _produces=mediatypes.APPLICATION_JSON)
        def logout(self):
            try:
                if self.app.header_token in self.request.headers:
                    token = self.request.headers[self.app.header_token]
                else:
                    raise Exception("Header '{header}' is required".format(header=self.app.header_token))
            except Exception as ex:
                self.app.log("info", self.app.service_name,
                             "Bad request: Access is denied due to invalid request",
                             exeption=ex, request=self.request)
                self.gen_http_error(400, "Bad request: The request must include a '{header}' " +
                                    "header field".format(header=self.app.header_token))
                return

            if self.app.db.delete_token(token) > 0:
                self.app.log("info", self.app.service_name, "Logged out", token=token)
            return dict({"status": "success"})

    class PermissionHandler(pyrestful.rest.RestHandler):
        def initialize(self, app):
            self.app = app

        @get(_path="/permission/{subsystem}", _types=[str], _produces=mediatypes.APPLICATION_JSON)
        def permission(self, subsystem):
            try:
                if self.app.header_token in self.request.headers:
                    token = self.request.headers[self.app.header_token]
                else:
                    raise Exception("Header '{header}' is required".format(header=self.app.header_token))
            except Exception as ex:
                self.app.log("info", self.app.service_name,
                             "Bad request: Access is denied due to invalid request",
                             exeption=ex, request=self.request)
                self.gen_http_error(400, "Bad request: The request must include a '{header}' " +
                                    "header field".format(header=self.app.header_token))
                return

            if self.app.db.refresh_token(token):
                permission = self.app.db.get_permission(token, subsystem)
                self.app.log("info", self.app.service_name, "Got permission", token=token, subsystem=subsystem,
                             permission=permission)
                return dict({"authorized": True, "level": permission})
            else:
                self.app.log("info", self.app.service_name,
                             "Unauthorized: Access is denied due to invalid token",
                             token=token, subsystem=subsystem)
                self.gen_http_error(403, "Unauthorized: Access is denied due to invalid token")
                return

    class TempHandler(pyrestful.rest.RestHandler):
        def initialize(self, database):
            self.database = database

        @get(_path="/temp/{login}/{password}", _types=[str, str], _produces=mediatypes.APPLICATION_JSON)
        def temp(self, login, password):
            p = self.request
            digest = base64.b64encode(
                hmac.new(password.encode('ascii'),
                         ("GET+/login/" + login + "/" + password + "+123456").encode('utf-8'),
                         hashlib.sha256).digest())
            return dict({"login": login, "password": password})

    def get_app(self):
        app = pyrestful.rest.RestService([self.LoginHandler, self.LogoutHandler, self.PermissionHandler],
                                         dict(app=self))
        return app
