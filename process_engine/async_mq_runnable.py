"""
    Класс асинхронного процесса с очередью сообщений
"""

import sys
import os
import pika, pika.exceptions
from pika.adapters.tornado_connection import TornadoConnection
import logging
import tornado
import tornado.ioloop

from process_engine.runnable import Runnable
from process_engine.response import Response


class AsyncMQRunnable(Runnable):
    def __init__(self, name, prefix=""):
        super().__init__(name, prefix=prefix)

        self.connection = None
        self.channel = None

        self.ioloop_instance = tornado.ioloop.IOLoop.instance()

        self.system_exchange_name = "{prefix}.system".format(prefix=self.prefix)
        self.target_exchange_name = "{prefix}.target".format(prefix=self.prefix)
        self.trap_exchange_name = "{prefix}.trap".format(prefix=self.prefix)

    def additional_init(self):
        super().additional_init()

        pika_logger = logging.getLogger("pika")
        pika_logger.setLevel(logging.CRITICAL)

        if "mq-broker" not in self.config:
            self.log("error", "MQ", "There is no configuration for MQ broker")
            raise Exception("There is no configuration for MQ broker")

    def runtime_cycle(self):
        super().runtime_cycle()

        self.log("info", "MQ",  "Connecting to",
            host=self.config["mq-broker"]["host"],
            port=int(self.config["mq-broker"]["port"])
        )

        self.mq_connect()

        if os.name == "nt":
            def periodical_run():
                pass
            if os.name == "nt":
                periodical_task = tornado.ioloop.PeriodicCallback(periodical_run, 1000, self.ioloop_instance)
                periodical_task.start()

        self.ioloop_instance.start()

    def stop(self):
        super().stop()
        if self.connection is not None:
            self.connection.close()
        self.ioloop_instance.stop()

    def mq_connect(self):
        credentials = pika.PlainCredentials(self.config["mq-broker"]["login"], self.config["mq-broker"]["password"])
        parameters = pika.ConnectionParameters(
            host=self.config["mq-broker"]["host"],
            port=int(self.config["mq-broker"]["port"]),
            virtual_host="/",
            credentials=credentials
        )
        self.connection = TornadoConnection(parameters,
                                            on_open_callback=self.on_mq_connect,
                                            on_open_error_callback=self.on_mq_open_error,
                                            on_close_callback=self.on_mq_closed)

    def on_mq_connect(self, connection):
        self.connection = connection
        self.log("info", "MQ", "Connected")

        self.connection.channel(self.on_mq_channel_open)

    def on_mq_closed(self, connection, reply_code, reply_text, callback=None):
        self.channel = None
        self.log("warning", "MQ", "Connection unexpectedly closed, reopening in 1 seconds")
        connection.add_timeout(1, self.mq_connect)

    def on_mq_open_error(self, connection, error_text):
        self.log("error", "MQ", error_text)
        self.stop()

    def on_mq_channel_open(self, channel):
        self.channel = channel

        # system
        self.channel.exchange_declare(exchange=self.system_exchange_name, type="topic")
        self.channel.queue_declare(queue=self.instance_service_name, exclusive=True,
                                   callback=self.on_system_queue_declared)

        # target
        self.channel.exchange_declare(exchange=self.target_exchange_name, type="direct")
        self.channel.queue_declare(queue=self.service_name, durable=True, callback=self.on_target_queue_declared)

        # trap
        self.channel.exchange_declare(exchange=self.trap_exchange_name, type="fanout")

    def on_system_queue_declared(self, *args, **kwargs):
        self.channel.queue_bind(exchange=self.system_exchange_name, queue=self.instance_service_name,
                                routing_key="{prefix}".
                                    format(prefix=self.prefix),
                                callback=self.on_system_queue_bind)
        self.channel.queue_bind(exchange=self.system_exchange_name, queue=self.instance_service_name,
                                routing_key="{service_name}".
                                    format(service_name=self.service_name),
                                callback=self.on_system_queue_bind)
        self.channel.queue_bind(exchange=self.system_exchange_name, queue=self.instance_service_name,
                                routing_key="{instance_service_name}".
                                    format(instance_service_name=self.instance_service_name),
                                callback=self.on_system_queue_bind)

    def on_system_queue_bind(self, *args, **kwargs):
        self.channel.basic_consume(self.on_system_message_recieved, queue=self.instance_service_name)

    def on_target_queue_declared(self, *args, **kwargs):
        self.channel.queue_bind(exchange=self.target_exchange_name, queue=self.service_name,
                                routing_key=self.service_name,
                                callback=self.on_target_queue_bind)
        self.channel.basic_qos(prefetch_count=1)

    def on_target_queue_bind(self, *args, **kwargs):
        self.channel.basic_consume(self.on_target_message_recieved, queue=self.service_name)

    def on_system_message_recieved(self, channel, method, properties, body):
        print("System message", channel, method, properties, body)
        channel.basic_ack(delivery_tag=method.delivery_tag)

        try:
            responses = self.process_system(body, reply_to=properties.reply_to, reply_id=properties.correlation_id)
        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.log("error", "Runtime", "Error",
                class_name=ex.__class__.__name__,
                error_text=ex.__repr__(),
                position=", ".join([fname, "{}".format(exc_tb.tb_lineno)])
            )
            return

        system_exchange_name = "{prefix}.system".format(prefix=self.prefix)
        for response in responses:
            if isinstance(response, Response) and response.to is not None and response.body is not None:
                if response.reply_id is not None:
                    self.channel.basic_publish(
                        exchange=system_exchange_name,
                        routing_key=response.to,
                        body=response.body,
                        properties=pika.BasicProperties(delivery_mode=2, correlation_id=response.reply_id)
                    )
                else:
                    self.channel.basic_publish(
                        exchange=system_exchange_name,
                        routing_key=response.to,
                        body=response.body,
                        properties=pika.BasicProperties(delivery_mode=2)
                    )

    def on_target_message_recieved(self, channel, method, properties, body):
        print("Target message", channel, method, properties, body)
        channel.basic_ack(delivery_tag=method.delivery_tag)

        try:
            responses = self.process_target(body, reply_to=properties.reply_to, reply_id=properties.correlation_id)
        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.log("error", "Runtime", "Error",
                class_name=ex.__class__.__name__,
                error_text=ex.__repr__(),
                position=", ".join([fname, "{}".format(exc_tb.tb_lineno)])
            )
            return

        target_exchange_name = "{prefix}.target".format(prefix=self.prefix)
        for response in responses:
            if isinstance(response, Response) and response.to is not None and response.body is not None:
                if response.reply_id is not None:
                    self.channel.basic_publish(
                        exchange=target_exchange_name,
                        routing_key=response.to,
                        body=response.body,
                        properties=pika.BasicProperties(delivery_mode=2, correlation_id=response.reply_id)
                    )
                else:
                    self.channel.basic_publish(
                        exchange=target_exchange_name,
                        routing_key=response.to,
                        body=response.body,
                        properties=pika.BasicProperties(delivery_mode=2)
                    )

    def process_system(self, request, reply_to, reply_id):
        if request == b"stop":
            self.stop()
        return []

    def process_target(self, request, reply_to, reply_id):
        return []

    def send_to_system(self, destination, body):
        system_exchange_name = "{prefix}.system".format(prefix=self.prefix)
        self.channel.basic_publish(
            exchange=system_exchange_name,
            routing_key=destination,
            body=body,
            properties=pika.BasicProperties(delivery_mode=2)
        )

    def send_to_target(self, destination, body):
        target_exchange_name = "{prefix}.target".format(prefix=self.prefix)
        self.channel.basic_publish(
            exchange=target_exchange_name,
            routing_key=destination,
            body=body,
            properties=pika.BasicProperties(delivery_mode=2)
        )

    def send_to_trap(self, body):
        trap_exchange_name = "{prefix}.trap".format(prefix=self.prefix)
        self.channel.basic_publish(
            exchange=trap_exchange_name,
            routing_key='',
            body=body,
            properties=pika.BasicProperties(delivery_mode=2)
        )
