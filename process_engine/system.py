"""
    Общая системная информация
"""


class System:
    name = "Process Engine"
    version = "1.0"

    prefix = "process_engine"

    conf_path = "./conf"

    log_path = "./logs"
    log_ext = "log"
    log_default_size = 1048576
    log_backup_count = 5

    data_path = "./data"

    subsystems = {}

    @staticmethod
    def register_service(service_app, name):
    # Регистрация сервисов, которым требуется аутентификация
        System.subsystems[name] = service_app
