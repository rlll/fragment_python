"""
    Обработчик-родитель для всех REST сервисов
"""

import pyrestful

from process_engine.database import AuthDb


class ServiceAppHandler(pyrestful.rest.RestHandler):

    def initialize(self, app):
        self.app = app
        self.level = None

    def pre_actions(self):
        try:
            self.level = self.app.get_permission(self.request.headers, self.app.subsystem_name)
        except self.app.TokenNotFoundException as ex:
            self.app.log("info", self.app.log_tag,
                         "Bad request: Access is denied due to invalid request",
                         exception=ex, request=self.request)
            self.gen_http_error(400, ("Bad request: The request must include a '{header}' " +
                                      "header field").format(header=self.app.header_token))
            return
        except self.app.NotAuthorizedException as ex:
            self.app.log("warning", self.app.log_tag,
                         "Unauthenticated: Access is denied due to invalid token",
                         exception=ex, request=self.request)
            self.gen_http_error(401, "Unauthenticated: Access is denied due to invalid token")
            return
        if self.level not in list(AuthDb.Permission.__members__.values()) or self.level == AuthDb.Permission.UNKNOWN:
            self.level = None
            self.app.log("info", self.app.log_tag,
                         "Internal Server Error: Unknown permissions level",
                         request=self.request)
            self.gen_http_error(501, "Internal Server Error: Unknown permissions level")
        elif self.level == AuthDb.Permission.NOT_ALLOWED:
            self.level = None
            self.app.log("info", self.app.log_tag,
                         "Forbidden: Access is not allowed",
                         request=self.request)
            self.gen_http_error(403, "Forbidden: Access is not allowed")
