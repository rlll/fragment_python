"""
    Параметры запуска утилиты run
"""

from process_engine.system import System
from process_engine import ConfigurationReader
from process_engine.configuration_readers import ConfigurationReaderEnv
from process_engine.configuration_readers import ConfigurationReaderFile
from process_engine.configuration_readers import ConfigurationReaderDb

from process_engine import ServiceAuthentication

def set_args(parser):
    subparser = parser.add_parser("service-authentication", help="run service authentication")

def get_runnable_instance(name, prefix, args):
    if name == "service-authentication":
        return ServiceAuthentication("service-authentication", prefix=prefix), "service-authentication"
    else:
        return None, ""


def get_configuration(runnable_name, source_uri=None, group=None, config_key=None):
    config = ConfigurationReader(config_key, group=group, source_uri=source_uri)
    if config.source_uri is None:
        result, content = ConfigurationReaderEnv.parse_source("source")
        if result:
            config.set_source_uri(content)
    if config.source_uri is None:
        result, content = ConfigurationReaderFile.parse_source(
            ConfigurationReaderFile.create_config_path(System.conf_path, "", "source.json")
        )
        if result:
            config.set_source_uri(content)
    if config.source_uri is None:
        config.set_source_uri("file:{path}".format(path=System.conf_path))

    if config.group is None:
        result, content = ConfigurationReaderEnv.parse_group("group")
        if result:
            config.group = content
    if config.group is None:
        result, content = ConfigurationReaderFile.parse_group(
            ConfigurationReaderFile.create_config_path(System.conf_path, "", "group.json")
        )
        if result:
            config.group = content
        else:
            return None, content

    if config.source.scheme == "env":
        if config.prefix is None:
            result, distribution = ConfigurationReaderEnv.parse_distribution("distribution")
            if result:
                if runnable_name in distribution:
                    config.prefix = distribution[runnable_name]
                else:
                    last_error = "Can't find {runnable_name} in distribution".format(runnable_name=runnable_name)
                    return None, last_error
            else:
                last_error = distribution
                return None, last_error
        return ConfigurationReaderEnv(config.prefix, group=config.group, source_uri=config.source_uri), ""
    elif config.source.scheme == "file":
        if config.prefix is None:
            result, distribution = ConfigurationReaderFile.parse_distribution(
                ConfigurationReaderFile.create_config_path(config.source.path, config.group, "distribution.json")
            )
            if result:
                if runnable_name in distribution:
                    config.prefix = distribution[runnable_name]
                else:
                    last_error = "Can't find {runnable_name} in distribution".format(
                        runnable_name=runnable_name
                    )
                    return None, last_error
            else:
                last_error = distribution
                return None, last_error
        return ConfigurationReaderFile(config.prefix, group=config.group, source_uri=config.source_uri), ""
    elif config.source.scheme == "mongodb":
        if config.prefix is None:
            result, distribution = ConfigurationReaderDb.parse_distribution(config.source_uri)
            if result:
                if runnable_name in distribution:
                    config.prefix = distribution[runnable_name]
                else:
                    last_error = "Can't find {runnable_name} in distribution".format(
                        runnable_name=runnable_name
                    )
                    return None, last_error
            else:
                last_error = distribution
                return None, last_error
        return ConfigurationReaderDb(config.prefix, group=config.group, source_uri=config.source_uri), ""

    return None, ""
